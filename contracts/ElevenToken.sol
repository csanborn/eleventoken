pragma solidity ^0.4.23;

import "./IERC20Token.sol";
import "./../node_modules/openzeppelin-solidity/contracts/token/ERC20/BasicToken.sol";
import "./../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "./../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract ElevenToken is ERC20, BasicToken, Ownable {
    string public constant name = "ElevenToken";
    string public constant symbol = "ET";
    uint256 private constant TOKEN_LIMIT = 1000000;
    address public saleContract;

    mapping (address => mapping (address => uint256)) internal allowed;

    event Mint(uint256 amount);

    constructor() public {
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
        require(_to != address(0));
        require(_value <= balances[_from]);
        require(_value <= allowed[_from][msg.sender]);

        balances[_from] = balances[_from].sub(_value);
        balances[_to] = balances[_to].add(_value);
        allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
        emit Transfer(_from, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public view returns (uint256) {
        return allowed[_owner][_spender];
    }

    function setSaleContract(address _contract) public onlyOwner {
        if (_contract != address(0)) {
            saleContract = _contract;
        }
    }

    function mint(address _holder, uint _value) public returns (bool) {
        require(msg.sender == saleContract);
        require(_value > 0);
        require(totalSupply_ + _value <= TOKEN_LIMIT);

        balances[_holder] += _value;
        totalSupply_ += _value;

        emit Mint(_value);
        emit Transfer(0x0, _holder, _value);

        return true;
    }
}
pragma solidity ^0.4.23;

// import "./ElevenToken.sol";
import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./IERC20Token.sol";
import "./../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
 * @title Eleven Token PreSale Contract
 * @dev Contract will control the pre-sale (no token ownership transfer).  The contract will allow the user (address) to 
 * sign up for a number of tokens to be distributed when distribution starts. It assumes payment was successful.  (No
 * ether transfer)
 */
contract ElevenTokenPreSale is Ownable {
    using SafeMath for uint256;

    enum PreSaleState { open, distrib, closed}

    IERC20Token public tokenContract;
    address[] public buyers;
    uint constant rounds = 4;
    mapping(address => uint256[4]) public purchased;
    uint256 constant PRESALE_LIMIT = 1000000;
    uint256 sold; 
    PreSaleState public state;
    uint public distributionRound = 0;

    event PreSold(address to, uint256 amount);
    event ContractStateChange(PreSaleState state);
    
    constructor(IERC20Token _tokenContract) public {
        tokenContract = _tokenContract;
        state = PreSaleState.open;
        emit ContractStateChange(state);
    }

    // this is only owner since the mechanics of the pre sale purchase is assumed to be off-chain.
    function allocate(address _to, uint256 _amount) public onlyOwner  {
        require(state == PreSaleState.open);
        require(_to != address(0));
        require(_amount > 0);
        require(sold + _amount < PRESALE_LIMIT);

        uint256[4] storage distribution = purchased[_to];

        if(distribution[0] == 0) {
            buyers.push(_to);
        }

        uint256 perRound = _amount.div(rounds);

        for(uint round = 0; round < rounds; round++) {
            distribution[round] += perRound; 
        }

        //Remainder in last round
        distribution[rounds - 1] += _amount % rounds;

        sold += _amount;

        emit PreSold(_to, _amount);
    } 

    function endSale() public onlyOwner {
        state = PreSaleState.distrib;
        emit ContractStateChange(state);
    }

    function available() public view returns (uint256) {
        return PRESALE_LIMIT - sold;
    }

    function buyerCount() public view returns(uint256) {
        return buyers.length;
    }

    function getPurchased(address _address) public view returns(uint256[4]) {
        return purchased[_address];
    }

    function distribute() public onlyOwner {
        require(state == PreSaleState.distrib);
        require(distributionRound < rounds);
        
        for(uint256 index = 0; index < buyers.length; index++) {
            address buyer = buyers[index];
            uint256 amount = purchased[buyer][distributionRound];

            require(tokenContract.mint(buyer, amount));
        }
        
        
        if(++distributionRound >= rounds) {
            state = PreSaleState.closed;
            emit ContractStateChange(state);
        }
    }

    
}

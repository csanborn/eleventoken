pragma solidity ^0.4.23;

// import "./ElevenToken.sol";
import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./IERC20Token.sol";
/**
 * @title Eleven Token Sale Contract
 * @dev Contract will control the sale.
 */
contract ElevenTokenSale {
    using SafeMath for uint256;

    IERC20Token public tokenContract;
    uint256 public price;
    address owner;
    uint256 public tokensSold;

    event Sold(address to, uint256 amount);
    
    constructor(IERC20Token _tokenContract, uint256 _price) public {
        
        owner = msg.sender;
        tokenContract = _tokenContract;        
        price = _price;
    }

    function buyTokens(uint256 numberOfTokens) public payable {
        
        require(msg.value == numberOfTokens.mul(price));

        require(tokenContract.mint(msg.sender, numberOfTokens));

        tokensSold += numberOfTokens;
        emit Sold(msg.sender, numberOfTokens);
    }

    function endSale() public {
        require(msg.sender == owner);

        require(tokenContract.transfer(owner, tokenContract.balanceOf(this)));

        msg.sender.transfer(address(this).balance);
    }

    
}
    

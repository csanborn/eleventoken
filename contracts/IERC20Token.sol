pragma solidity ^0.4.23;

interface IERC20Token {
    function balanceOf(address owner) external returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function setSaleContract(address _contract) external;
    function mint(address _holder, uint _value) external returns (bool);
}
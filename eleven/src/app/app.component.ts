import { Component } from '@angular/core';
import { ElevenTokenService } from './contractServices/elevenToken.contract.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Token Test';
  address: string;
  name: string;
  balance: number;

  constructor(private elevenToken: ElevenTokenService ) {
    elevenToken.getAccount().then(addr => this.address = addr);
    elevenToken.getBalance().then(bal => this.balance = bal);
  }
}

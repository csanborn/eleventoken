import { Injectable } from '@angular/core';
import * as Web3 from 'web3';
declare let window: any;

const contracts = require('./contracts/contracts.json');

@Injectable()
export class ContractServiceFactory {
  private _web3: any = null;

  private _tokenContract: any = null;
  private _saleContract: any = null;
  private _preSaleContract: any = null;


  constructor() {
  }

  private get web3(): any {
    if (this._web3 === null) {
      if (typeof window.web3 !== 'undefined') {
        this._web3 = new Web3(window.web3.currentProvider);
      } else {
        console.warn(
          'Please use a dapp browser like mist or MetaMask plugin for chrome'
        );
      }
    }
    return this._web3;
  }

  public get tokenContract(): any {
    if (this._tokenContract === null) {
      this._tokenContract = this.createContact('token');
    }

    return this._tokenContract;
  }

  public get saleContract(): any {
    if (this._saleContract === null) {
      this._saleContract = this.createContact('sale');
    }

    return this._saleContract;
  }

  public get preSaleContract(): any {
    if (this._preSaleContract === null) {
      this._preSaleContract = this.createContact('preSale');
    }

    return this._preSaleContract;
  }

  private createContact(name: string): any {
    const metaData = contracts[name];
    return this._web3.eth.contract(metaData.abi).at(metaData.address);
  }
}

import { NgModule } from '@angular/core';
import { ElevenTokenService } from './elevenToken.contract.service';

@NgModule({
  declarations: [],
  imports: [ ],
  providers: [
    ElevenTokenService
  ],
  bootstrap: [],
  exports: []
})
export class ContractsModule { }

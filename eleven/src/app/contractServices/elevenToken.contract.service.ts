import { Injectable } from '@angular/core';
import * as Web3 from 'web3';
import { ContractServiceFactory } from './contract.service.factory';
import { AddressService } from './address.service';

declare let window: any;

@Injectable()
export class ElevenTokenService {
  private _contract: any;

  constructor(contractsFactory: ContractServiceFactory, private addressService: AddressService) {
    this._contract = contractsFactory.tokenContract;
  }

  public async getBalance(): Promise<number> {
    const account = await this.addressService.getAccount();
    return new Promise((resolve, reject) => {
      this._contract.balanceOf.call(account, (err, result) => {
        if (err != null ) {
          reject(err);
        }
        resolve(result);
      });
    }) as Promise<number>;
  }
}

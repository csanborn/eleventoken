import { Injectable } from '@angular/core';
import * as Web3 from 'web3';
import { ContractServiceFactory } from './contract.service.factory';

const tokenAbi = require('./contracts/ElevenTokenPreSale.json');
declare let window: any;

@Injectable()
export class PreSaleService {
  private _contract: any;
  private _account: string = null;
  private _web3: any;

  constructor(factory: ContractServiceFactory) {
    this._contract = factory.preSaleContract;
  }
}

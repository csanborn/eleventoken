var gulp = require('gulp');
//var exec = require('gulp-exec');
var exec = require('child_process').exec;
var del = require("del");
var fs = require("fs");

gulp.task('deploy', ["clean"], function() {
  exec('truffle deploy --network development')
});


gulp.task('clean', function() {
  return del('build');
});

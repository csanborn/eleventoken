const ElevenToken = artifacts.require("./ElevenToken.sol");
const ElevenTokenSale = artifacts.require("./ElevenTokenSale.sol");
const ElevenTokenPreSale = artifacts.require("./ElevenTokenPreSale.sol");
const fs = require("fs");


module.exports = function(deployer, network, accounts) {
  deployer.deploy(ElevenToken)
    .then(() => ElevenToken.deployed())
    .then(eleven => {
      

      deployer.deploy(ElevenTokenSale, ElevenToken.address, 10000000000000000).then(() => {
        deployer.deploy(ElevenTokenPreSale, ElevenToken.address).then(() => {

          let path = './eleven/src/app/contractServices/contracts/contracts.json';
          
           let contracts = {
            token: {
              address: ElevenToken.address,
              abi: ElevenToken.abi
            },
            preSale: {
              address: ElevenTokenPreSale.address,
              abi: ElevenTokenPreSale.abi
            }, 
            sale: { 
              address: ElevenTokenSale.address, 
              abi: ElevenTokenSale.abi
            }
           };

           

          fs.writeFileSync(path, JSON.stringify(contracts), {flag: 'w'});
        });
      });

      

      

    })
    

};

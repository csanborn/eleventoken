const Eleven = artifacts.require("ElevenToken");
var _ = require("lodash");


contract('ElevenToken Tests', async (accounts) => {

  const initialSupply = 1000000;

  it("should allow approve and transfer from", async() => {
    const transferAmount = 100;
    let eleven = await Eleven.deployed();
    await eleven.setSaleContract(accounts[0]);
    await eleven.mint(accounts[0], initialSupply);

    let approveTx = await eleven.approve(accounts[1], transferAmount, {from: accounts[0]});
    
    let allowance = await eleven.allowance.call(accounts[0], accounts[1]);
    assert.equal(allowance.toNumber(), transferAmount);

    let transferTx = await eleven.transferFrom(accounts[0], accounts[1], transferAmount, {from: accounts[1]});

    let balance = await eleven.balanceOf.call(accounts[0]);
    assert.equal(balance.toNumber(), initialSupply - transferAmount);

    let balance2 = await eleven.balanceOf.call(accounts[1]);
    assert.equal(balance2.toNumber(), transferAmount);
  });

  function logEvents(tx) {
    _.forEach(tx.logs, (log) => {
      var output = log.event + " args: {\n";
      _.forIn(log.args, (value, key) => output += "\t" + key + ": " + value + "\n");
      output += "}\n"
      console.log(output);
    }); 
  }

});
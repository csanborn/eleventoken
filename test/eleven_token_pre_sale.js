const PreSale = artifacts.require("ElevenTokenPreSale");
const Eleven = artifacts.require("ElevenToken");

contract('ElevenTokenPreSale', async (accounts)  => {

  it("should distribute across rounds correctly", async () => {
    let preSale = await PreSale.deployed();

    await preSale.allocate.sendTransaction(accounts[2], 10);

    let buyer = await preSale.buyers.call(0);

    assert.equal(buyer, accounts[2]);

    let purchased = await preSale.getPurchased.call(buyer);

    assert.lengthOf(purchased, 4);
    assert.equal(purchased[0].toNumber(), 2);
    assert.equal(purchased[1].toNumber(), 2);
    assert.equal(purchased[2].toNumber(), 2);
    assert.equal(purchased[3].toNumber(), 4);

    await preSale.allocate.sendTransaction(accounts[2], 20);

    purchased = await preSale.getPurchased.call(buyer);

    assert.equal(purchased[0].toNumber(), 7);
    assert.equal(purchased[1].toNumber(), 7);
    assert.equal(purchased[2].toNumber(), 7);
    assert.equal(purchased[3].toNumber(), 9);
  });

  it("should be able to distribute each round", async () => {
    let preSale = await PreSale.deployed();
    let eleven = await Eleven.deployed();

    await eleven.setSaleContract.sendTransaction(preSale.address);

    assert.equal((await preSale.state.call()).toNumber(), 0, "State should be open");
    
    await preSale.allocate.sendTransaction(accounts[3], 40);
    await preSale.allocate.sendTransaction(accounts[4], 20);
    await preSale.allocate.sendTransaction(accounts[5], 10);

    await preSale.endSale.sendTransaction();
    assert.equal((await preSale.state.call()).toNumber(), 1, "State should be distrib");
    await preSale.distribute.sendTransaction();

    assert.equal((await eleven.balanceOf.call(accounts[3])).toNumber(), 10);
    assert.equal((await eleven.balanceOf.call(accounts[4])).toNumber(), 5);
    assert.equal((await eleven.balanceOf.call(accounts[5])).toNumber(), 2);

    await preSale.distribute.sendTransaction();

    assert.equal((await eleven.balanceOf.call(accounts[3])).toNumber(), 20);
    assert.equal((await eleven.balanceOf.call(accounts[4])).toNumber(), 10);
    assert.equal((await eleven.balanceOf.call(accounts[5])).toNumber(), 4);

    await preSale.distribute.sendTransaction();

    assert.equal((await eleven.balanceOf.call(accounts[3])).toNumber(), 30);
    assert.equal((await eleven.balanceOf.call(accounts[4])).toNumber(), 15);
    assert.equal((await eleven.balanceOf.call(accounts[5])).toNumber(), 6);

    await preSale.distribute.sendTransaction();

    assert.equal((await preSale.state.call()).toNumber(), 2, "State should be closed");
    assert.equal((await eleven.balanceOf.call(accounts[3])).toNumber(), 40);
    assert.equal((await eleven.balanceOf.call(accounts[4])).toNumber(), 20);
    assert.equal((await eleven.balanceOf.call(accounts[5])).toNumber(), 10);


  });

  function getState(stateNum) {
    switch(stateNum) {
      case 0: return "open";
      case 1: return "distrib";
      case 2: return "closed";
      default: return null;
    }
  }

  function registerEventLogging(error, event) {
    console.log(event); 
  }
});


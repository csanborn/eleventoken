const Sale = artifacts.require("ElevenTokenSale");
const Eleven = artifacts.require("ElevenToken");

contract('ElevenTokenSale', async (accounts) => {
  const salePrice = 10000000000000000;

  it("should have price", async () =>  {
    let tokenSale = await Sale.deployed();
    
    let price = await tokenSale.price.call();

    assert.equal(price, salePrice);
  });


  it("should allow sale of one token", async () => {
    const qty = 1;

    let eleven = await Eleven.deployed();
    let supply = await eleven.totalSupply.call();

    let tokenSale = await Sale.deployed();

    await eleven.setSaleContract(tokenSale.address)

    let tx = await tokenSale.buyTokens(qty, { from: accounts[1], value: salePrice * qty });
        
    let balance1 = await eleven.balanceOf.call(accounts[1]);
    assert.equal(balance1, qty);
  });


  it("should allow sale of 50 tokens 100 times and close sale", async () => {
    const qty = 50;
    const iterations = 1;
    const fromAccount = accounts[1];

    let eleven = await Eleven.deployed();
    let supply = await eleven.totalSupply.call();
    let gasCost;

    let ownerinitialBalance = web3.fromWei(web3.eth.getBalance(accounts[0]));

    let tokenSale = await Sale.deployed();
    await eleven.setSaleContract(tokenSale.address);

    for(let x = 0; x < iterations; x++) {
      let tx = await tokenSale.buyTokens.sendTransaction(qty, { from: fromAccount, value: salePrice * qty });
      //console.log(tx);
    }
        
    let balance1 = await eleven.balanceOf.call(fromAccount);
    assert.isAtLeast(balance1.toNumber(), qty * iterations);

    let endtx = await tokenSale.endSale.sendTransaction();
    var receipt = await web3.eth.getTransactionReceipt(endtx);
    console.log(receipt.gasUsed);
    
    let ownerFinishBalance = web3.fromWei(web3.eth.getBalance(accounts[0]));

    console.log("Ending Balance:", ownerFinishBalance.toNumber());
    // console.log("Gas Sum:", gasCost);
    // console.log("Total Gas:", endTx.cumulativeGasUsed.toNumber());

    assert.isAbove(ownerFinishBalance.toNumber(), ownerinitialBalance.toNumber())

  });

  function logEvents(tx) {
    _.forEach(tx.logs, (log) => {
      var output = log.event + " args: {\n";
      _.forIn(log.args, (value, key) => output += "\t" + key + ": " + value + "\n");
      output += "}\n"
      console.log(output);
    }); 
  }
});
